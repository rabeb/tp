# Séance 5

## Chainage des commandes
Avec des commandes de type `grep`, `sed`, `awk`, `sort`, `uniq` affichez un listing des packages utilisés par un projet node en parcourant le contenu du répertoire  node_modules de ce projet

## Suivre l'évolution d'un dépot github qu'on a forké

### Fork

Sur github, forker le projet test-git git@github.com:[other_user]/test-git.git

### Récupérer le fork sur son poste

Bien nommer le clone avec le nom de son user github en préfix (remplacer [my_user] par votre user github)

```
$ git clone git@github.com:[my_user]/test-git.git my_user_test-git.git
$ cd my_user_test-git.git
```

### Ajouter l'upstream du projet initial

On vérifie qu'on est bien dans son fork :

```
$ git remote -v
origin	git@github.com:[my_user]/test-git.git (fetch)
origin	git@github.com:[my_user]/test-git.git (push)
```

Puis on ajoute le dépot d'origine sous la dénomination 'upstream'

```
$ git remote add upstream git@github.com:[other_user]/test-git.git
$ git remote -vgit remote -v
origin	git@github.com:[my_user]/test-git.git (fetch)
origin	git@github.com:[my_user]/test-git.git (push)
upstream	git@github.com:[other_user]/test-git.git (fetch)
upstream	git@github.com:[other_user]/test-git.git (push)
```

### Se mettre à jour

Mettre à jour sa branche master avec ce qui a été fait sur 'upstream' :

```
$ git fetch upstream
$ git checkout master
$ git merge upstream/master
```

### Pousser le résultat dans son dépot :

A ce stade les modifications ne sont que locales, donc on les pousse sur son dépot github

```
$ git push
```
