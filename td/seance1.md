# TD EDL

## Installer oh-my-zsh, et le configurer
oh-my-zsh rendra votre prompt beau et intelligent

1. Consulter le source du script d'install https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh
1. lancer la commande qui va tout installer  
```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```
1. Configurer son theme, et ajouter des plugins utiles en éditant ~/.zshrc

```
THEME="candy"

...

plugins=(git git-extra debian python pylint pep8 nvm)
```

## Activer NVM dans zsh

NVM est le gestionnaire de version de node, il permet notament de changer de version de node en fonction du projet sur lequel on travail
1. Télécharger le script d'install
```
curl https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh -o /tmp/install-nvm.sh
```
2. Lire le script (`ESC` suivi de `:q` suivi de `ENTER` pour sortir)
```
$ vim /tmp/install-nvm.sh
```
3. Attribuer les droits d'execution au script
```
$ chmod +x /tmp/install-nvm.sh
```
4. lancer le script
```
/tmp/install-nvm.sh
```
5. dans son .zshrc, ajouter ce code

```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

autoload -U add-zsh-hook
load-nvmrc() {
  if [[ -f .nvmrc && -r .nvmrc ]]; then
    nvm use
  elif [[ $(nvm version) != $(nvm version default)  ]]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
```
6. relancer un shell et installer la dernière version LTS de node (nvm ls-remote pour avoir la liste des version disponibles)
```
nvm install v6.9.5
```

## Minitwit
1. Cloner flask
```
$ mkdir ~/td_edl
$ cd ~/td_edl
$ git clone https://github.com/pallets/flask.git
$ cd flask
```
2. Créer un envirronement virtuel python  
```
$ virtualenv venvflask
$ source ./venvflask/bin/activate
```
3. Aller dans le répertoire de miniwit et l'installer dans son venvflask
```
(venvflask) $ cd examples/minitwit
(venvflask) $ pip install --editable .
```
4. Configurer flask pour qu'il lance miniwit
```
(venvflask) $ export FLASK_APP=minitwit
```
5. Initialiser la base de donnée et lancer flask
```
(venvflask) $ flask initdb
Initialized the database.
(venvflask) $ flask run
 * Serving Flask app "minitwit"
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```
6. Ouvrez votre navigateur sur la page http://127.0.0.1:5000/
