# Séance 3

## Introduction à VIM
Commandes de base :

Mode commande `ESC`  
Mode insertion `i` (insert)  
Mode visuel `v` (visual)  
Mode visuel 'carré' ctrl-V  

Sortir sans enregistrer `ESC` `:q!`  (quit!)  
Sortir en enregistrant `ESC` `:wq` ou `ESC` `:x`  
Enregistrer `ESC` `:w`  

Defaire `ESC` `u` (undo)   

Copier une ligne `ESC` `yy` (yank)  
Coller une ligne `ESC` `p` ou `ESC` `P`  

Suivre les instructions du professeur, et noter les commandes essentielles.

Vim propose un tutoriel, lancez vimtutor dans un terminal pour le suivre.

## Introduction à Sed

Sed permet, entre autres, d'effectuer des modifications sur les contenus des fichiers textes.
Cloner mcf-js sur sa machine et faire les transformations nécessaires pour le renommer

1. Créer une branche feat-renaming
        $ git checkout -b feat-renaming
2. Dans cette branche, chercher les occurences de mcf avec grep :
        $ grep -i -r mcf **/*.js

3. utiliser la commande de remplacement de sed pour effectuer les modifications
        sed -i -e 's/texte à remplacer/texte de remplacement/g' fichier_à_modifier.ext
4. Utiliser `git diff` et la commande grep pour voir l'avancement du travail
5. Utiliser `npm run test` pour vérifier que le résultat fonctionne toujours

## Introduction à Docker

Pour des raisons de sécurité, on va utiliser VirtualBox sur les machines de TD pour faire tourner docker. Attendez les instructions du professeurs pour lancer la VM virtualbox et prendre connaissance des commandes de base docker.

Nous allons voir comment docker permet d'utiliser des applications autonommes.

Récupérer l'image de nginx et la lancer en tâche de fond.

```
$ docker pull nginx
$ docker run --name mynginx1 -p8080:80 -d nginx
```
Utiliser Owasp pour vérifier la qualité de l'appli d'un point de vue sécurité

```
$ docker pull owasp/zap2docker-stable
$ docker run -t owasp/zap2docker-stable zap-baseline.py -t http://localhost:8080/
```


## Suivre l'évolution d'un dépot github qu'on a forké

### Fork

Sur github, forker le projet test-git git@github.com:[other_user]/test-git.git

### Récupérer le fork sur son poste

Bien nommer le clone avec le nom de son user github en préfix (remplacer [my_user] par votre user github)

```
$ git clone git@github.com:[my_user]/test-git.git my_user_test-git.git
$ cd my_user_test-git.git
```

### Ajouter l'upstream du projet initial

On vérifie qu'on est bien dans son fork :

```
$ git remote -v
origin	git@github.com:[my_user]/test-git.git (fetch)
origin	git@github.com:[my_user]/test-git.git (push)
```

Puis on ajoute le dépot d'origine sous la dénomination 'upstream'

```
$ git remote add upstream git@github.com:[other_user]/test-git.git
$ git remote -vgit remote -v
origin	git@github.com:[my_user]/test-git.git (fetch)
origin	git@github.com:[my_user]/test-git.git (push)
upstream	git@github.com:[other_user]/test-git.git (fetch)
upstream	git@github.com:[other_user]/test-git.git (push)
```

### Se mettre à jour

Mettre à jour sa branche master avec ce qui a été fait sur 'upstream' :

```
$ git fetch upstream
$ git checkout master
$ git merge upstream/master
```

### Pousser le résultat dans son dépot :

A ce stade les modifications ne sont que locales, donc on les pousse sur son dépot github

```
$ git push
```


