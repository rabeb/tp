# Séance 4

## Protection de son poste de travail
### sudo
Limiter l'utilisation du compte root, c'est  aussi limiter le risque d' endommager son système. Mais sur un poste de developpement il est souvent nécessaire d'arrêter/lancer des services ou éditer leurs configurations, sudo est pour cela nécessaire.
Sudo est souvent installé par défaut (sur les dérivées d'ubuntu par exemple). S'il n'est pas installé (sous debian par exemple), installez le et configurez le :
```
# apt-get install sudo
# visudo
```
Ajouter votre compte sous celui de root :
```
# User privilege specification
root    ALL=(ALL:ALL) ALL
eric    ALL=(ALL:ALL) ALL
```

### firewall

Lorsque nous développons, nous utilisons des briques applicatives qui peuvent présenter une faille de sécurité pour notre système, ou divulguer des informations que nous ne souhaitons faire fuiter (via les bases de données que nous allons utiliser par exemple).

Il est essentiel d'avoir un firewall correctement configuré.  
Le noyau linux embarque un firewall : netfilter. Mais celui-ci est devenu très compliqué à configurer, on utilisera un programme pour nous aider à le faire.
- Sur un ordinateur de bureau ou portable on peut utiliser `ufw` et son interface graphique `gufw`
- sur un serveur : `firehol`

Pour savoir si votre firewall est activé, lancez cette commande dans une console :

```
$ sudo iptables -L
```

Pour tester votre firewall depuis un autre ordinateur :

```
nmap -P0 IP_DU_SERVEUR
```

## Outils de diagnostique
- Diagnostique erreur d'exécution système :  
  strace
- Diagnostique des processus en cours :  
  ps, top, htop
- Diagnostique des ports utilisés :  
  telnet, netcat, netstat, nmap
- Diagnostique du trafic réseau :  
  wireshark
- Examen des logs :  
  tail, less

## Python : virtualenvwrapper
virtualenvwrapper permet de créer des envirronements dans lesquels on peut avoir des versions différentes de paquets python.

1. Installer virtualenvwrapper

        $ sudo apt-get install virtualenvwrapper

2. Créer le répertoire qui contiendra les virtualenvs:

        $ mkdir $HOME/.virtualenvs

3. Ajoutez ces lignes dans votre .zshrc ou .bashrc

        export WORKON_HOME=$HOME/.virtualenvs
        source /usr/share/virtualenvwrapper/virtualenvwrapper.sh

Que font ces lignes ?


## Minitwit
  1. Cloner flask
  ```
  $ mkdir ~/td_edl
  $ cd ~/td_edl
  $ git clone https://github.com/pallets/flask.git
  $ cd flask
  ```
  2. Créer un envirronement virtuel python  
  ```
  $ mkvirtualenv minitwit
  ```
  3. Aller dans le répertoire de minitwit et l'installer dans son virtualenv
  ```
  (minitwit) $ cd examples/minitwit
  (minitwit) $ pip install --editable .
  ```

  4. Configurer flask pour qu'il lance miniwit
  ```
  (minitwit) $ export FLASK_APP=minitwit
  ```
  5. Initialiser la base de donnée et lancer flask
  ```
  (minitwit) $ flask initdb
  Initialized the database.
  (minitwit) $ flask run
   * Serving Flask app "minitwit"
   * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
  ```

  6. Ouvrez votre navigateur sur la page http://127.0.0.1:5000/

### Ajouter un frontal nginx
1. Installer nginx

        sudo apt-get install nginx
2. Copier le fichier de configuration par defaut et l'éditer

        $ sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/twit.local.dev
        $ sudo vim /etc/nginx/sites-available/twit.local.dev
3. Activer le site en créant un lien dans sites-enabled
          sudo ln -s /etc/nginx/sites-available/twit.local.dev /etc/nginx/sites-enabled/twit.local.dev
4. Tester sa configurations

        $ sudo nginx -t
4. Recharger nginx

        $ sudo systemctl reload nginx

## Introduction à Sed

Sed permet, entre autres, d'effectuer des modifications sur les contenus des fichiers textes.
Cloner mcf-js sur sa machine et faire les transformations nécessaires pour le renommer

1. Créer une branche feat-renaming

        $ git checkout -b feat-renaming

2. Dans cette branche, chercher les occurences de mcf avec grep :

        $ grep -i -r mcf **/*.js

3. utiliser la commande de remplacement de sed pour effectuer les modifications

        sed -i -e 's/texte à remplacer/texte de remplacement/g' fichier_à_modifier.ext

4. Utiliser `git diff` et la commande grep pour voir l'avancement du travail
5. Utiliser `npm run test` pour vérifier que le résultat fonctionne toujours
