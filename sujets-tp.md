Sujets de TP
============

Version : 2018-02-04

Les sujets de TP sont mis à jour chaque année.


Consignes
---------

### Un élève pour un sujet

Pour la matière EDL il n'y a pas de binome pour les TP.
Chaque élève doit choisir son/ses sujets de TP seul, doit réaliser son/ses
sujets de TP seul, doit effectuer son/ses *Pull Request* (PR) seul.

Les élèves sont autorisés à demander de l'aide à leurs camarades de classe, mais
l'évaluation est individuelle.

### La référence

Les élèves doivent se servir du paquet [mcf](https://github.com/madarche/mcf-js)
comme référence, comme exemple pour toutes les bonnes pratiques à jour à
suivre.

### Bonnes pratiques

* Les modifications dans un projet doivent être réalisées :

    * dans un commit unique (utiliser `git rebase` comme vu en cours pour
      fusionner des commits successifs si vous êtes amenés à faire plusieurs
      commits)

    * dans une branche dédiée (la branche doit avoir un nom porteur de sens,
      adapté, et tout en minuscule), par exemple `fix-column-check`,
      `feat-dependency-update`, `feat-eslint-switch` etc.

* Les commits doivent impérativement suivre les *règles des commits Git* :
    * [article 1](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
    * [article2](https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message)

* Quand dans un projet des fonctionnalités sont ajoutées (par exemple ajout de
nouvelles méthodes, etc.) ou que le comportement est modifié (modification de
méthodes, changement des options par défaut, etc.) if faut mettre à jour la
documentation et, si il y en a, généralement dans un répertoire `test` ou
`tests`) les tests unitaires du projet.

### Validation de vos PR

Les élèves ne doivent **jamais** envoyer une PR à un projet sans avoir fait
valider cette PR dans leur fourche (fork) du projet par un professeur.


Notation des TP
---------------

### Critères de notation

* Conformité de la PR aux *règles des commits Git*
  cf. [Bonnes pratiques](#bonnes-pratiques)
* Empathie avec les auteurs du logiciel (se renseigner sur le projet et lire les
  PR effectuées par d'autres, certains auteurs aiment un style très strict,
  d'autres un style décontracté, etc.)
* Qualité de la rédaction du message en anglais
* Le *merge* d'une *Pull Request* (PR), c'est à dire l'approbation de la PR par
  l'auteur du logiciel

### Rapport de TP

Il faut écrire un rapport minimaliste et l'envoyer par courriel aux professeurs.
Le rapport peut être un simple fichier en Markdown, ou en HTML, ou
éventuellement un fichier ODT.

Ce rapport doit juste indiquer :

* l'URL du compte GitHub/GitLab/autre utilisé par l'élève
* le ou les URL des tickets et de la ou des *Pull Requests*/*Merge Requests* en
  indiquant pour chaque :
    * ce qui a été réalisé par l'élève
      (environ 1 paragraphe)
    * ce qui accepté/refusé par le(s) mainteneur(s) du projet
      (environ 1 paragraphe)

Les professeurs n'iront pas deviner où se trouvent les contributions des élèves,
c'est uniquement sur la base de ce rapport que les contributions seront lues et
évaluées.


Sujets
------

Les sujets ci-dessous sont proposés.

Choisir un projet distribué suivant une licence libre sur une forge logicielle
collaborative comme https://framagit.org/ , https://gitlab.com/explore/groups ,
ou https://github.com/ et y réaliser des contributions significatives du même
niveau de difficulté que les autres sujets ci-dessus.

Le sujet choisi par l'élève devra être validé par le professeur avant que
l'élève ne commence son travail de contribution.


[Watch](https://github.com/mikeal/watch)
----------------------------------------

Résoudre le ticket [Create automated tests](https://github.com/mikeal/watch/issues/87)

[chai-http](https://github.com/chaijs/chai-http)
------------------------------------------------

* Ajout des badges (npm, dependencies, devDependencies)

* Ajout de ESLint

* Ajout de ESLint dans les tests


[Stupid-Table-Plugin](https://github.com/joequery/Stupid-Table-Plugin)
----------------------------------------------------------------------

* Ajout de tests ESLint => attribué à ASM

* Génération le minifié .min.js par NPM depuis le fichier fichier `package.json`
  à l'aide d'un script `build` => attribué à RAR


[CookieJar](https://github.com/bmeck/node-cookiejar)
----------------------------------------------------

* Amélioration du balisage markdown du fichier `readme.md` en remplaçant les
  endroits en `pre` par une syntaxe plus adaptée => Attribué à ABE

* Documenter la méthode `CookieAccessInfo.All`

* Passage à ESLint à la place de JShint

* Ajout des badges (npm, devDependencies) => Attribué à NSE

* Ajouter la possibilité que le constructeur de `Cookie` accepte un objet
  `{name, value, expiration_date, path, domain, secure, httponly}` comme argument

* Ajout d'un build d'intégration continue avec le badge associé

    Actuellement il n'y a pas de build automatique de l'intégration continue.
    Quand un build de l'intégration continue fonctionne, il y a un pictogramme
    vert qui indique `build passing` comme sur
    https://github.com/madarche/mcf-js .
    Pour que le build passe, il faut que l'auteur du projet se crée un compte
    sur http://travis-ci.org/ et autorise Travis à accéder à son compte GitHub
    et au dépôt Git du projet. Pour arriver à expliquer cela à l'auteur, il faut
    au préalable que l'étudiant réalise les manipulations de son côté et
    comprenne comment cela fonctionne en mettant en place l'intégration continue
    sur son projet fourché (fork) de manière à avoir un `build passing`.

* Ajout de la couverture de tests avec ajout du badge coverage


[Nunjucks](https://github.com/mozilla/nunjucks)
----------------------------------------------------

* Dans le templating, ajouter le support de
  l'[opératieur `in`](http://jinja.pocoo.org/docs/2.9/templates/#other-operators)


[Convict](https://github.com/mozilla/node-convict)
--------------------------------------------------

* Colorier les avertissements de l'option `allowed: 'warn'` dans une couleur
  voyante, par exemple jaune comme cela est réalisé par le paquet NPM
  [depd](https://www.npmjs.com/package/depd) => Attribué à NBE

* Réorganiser les tests unitaires (pour les rendre plus clairs et plus
  maintenables, cela implique des renommages de fichiers et éventuellement la
  suppression ou la création de nouveaux fichiers de tests)


[repolint](https://github.com/madarche/npm-repolint)
----------------------------------------------------

* Mettre à jour le fichier README.md pour expliquer en anglais l'objectif du
  projet => attribué à DGA

    Le programme doit générer un rapport de qualité sur n projets de paquet NPM
    sur GitHub (en utilisant l'API de GitHub) pour les projets :

    * n'ayant pas de description dans le fichier `package.json`
    * n'ayant pas de licence dans le fichier `package.json`
    * n'ayant pas de fichier `LICENSE` à la racine du projet
    * n'ayant pas de fichier `.editorconfig` à la racine du projet
    * n'ayant pas de champ `repository` dans le fichier `package.json`
    * n'ayant pas de badge NPM dans le fichier `README.md`
    * n'ayant pas de badge dependencies dans le fichier `README.md`
    * n'ayant pas de badge devDependencies dans le fichier `README.md`
    * n'ayant pas de test ESLint

* Ajouter l'intégration continue

* Générer les rapports dans un répertoire en dehors des sources

* Utiliser le paquet `yargs-parser` pour gérer les arguments passés en ligne de
  commande au programme


[Libxmljs](https://github.com/libxmljs/libxmljs)
----------------------------------------------------

* Ajout de la couverture de tests avec ajout du badge coverage


[SSPK](https://github.com/e-henry/sspk)
----------------------------------------------------

* Valider les comptes github des entrées du fichier => Attribué à ACH
* Valider la clef SSH des entrées du fichier (simple format ou appel
à ssh-keygen)

[insert-css](https://github.com/substack/insert-css)
----------------------------------------------------

* Ajout du badge npm, en reprise de la PR
  https://github.com/substack/insert-css/pull/30 faite précédemment par un autre
  élève

* Ajout de ESLint dans les tests

* Ajout de la couverture de tests avec ajout du badge coverage


[Npm-check-github-stars](https://github.com/OlivierCoilland/npm-check-github-stars)
-----------------------------------------------------------------------------------

* Ajout de tests fonctionnels

* Ajout de couleurs dans la sortie du programme avec le paquet
  [colors](https://www.npmjs.com/package/colors) => attribué à OEL

* Ajout des badges (npm, dependencies, devDependencies)

* Ajout de la couverture de tests avec ajout du badge coverage


[Watch](https://github.com/mikeal/watch)
--------------------------------------------

* Ajout des badges (npm, dependencies, devDependencies) => Attribué à ADA

* Ajout de ESLint dans les tests => Attribué à ANA


[Simple-cookie](https://github.com/juji/simple-cookie)
------------------------------------------------------

* MAJ devDependencies => Attribué à EUL

* Ajout de la couverture de tests avec ajout du badge coverage


[Mocha](https://github.com/mochajs/mochajs.github.io)
------------------------------------------------------

* Documenter dans https://mochajs.org/ l'utilisation de « `mocha` vs `_mocha` ».
  Se baser sur la pratique et la compréhension du code https://github.com/mochajs/mocha

  Faire une nouvelle PR en se basant sur le texte suivant fait dans une PR
  non-aboutie d'un élève d'une année précédente
  https://github.com/mochajs/old-mochajs-site/pull/77/commits/3f7ccfaa446ddc07b5e2db44c52fc5d4e463d338#diff-d680e8a854a7cbad6d490c445cba2ebaR1236


[Tracer](https://github.com/baryon/tracer)
------------------------------------------------------

* MAJ devDependencies

* Rendre le `.eslintrc` beaucoup plus exigeant en terme de qualité, actuellement
  il vérifie uniquement la présence de l'instruction `use strict`. => Attribué à AEL


[Sprintf.js](https://github.com/alexei/sprintf.js)
------------------------------------------------------

* MAJ des devDependencies => OOU


[RandomColor](https://github.com/davidmerfield/RandomColor)
-----------------------------------------------------------

* Finaliser la PR https://github.com/davidmerfield/randomColor/pull/87 commencée
  par un élève d'une année précédente => Attribué à MBO

* Réaliser les points *To do*


[Js-must](https://github.com/moll/js-must/)
------------------------------------------------------

* Amélioration du message d'erreur pour l'assertion `length()` en y ajoutant la
  différence entre la longueur attendue et la longuer actuelle.  => attribué à HHR

  Par exemple avec le code suivant :
  ```javascript
  [2,'a'].must.have.length(3)
  ```

  Au lieu de :
  ```
  AssertionError: [2,"a"] must have length of 3
  ```
  il faudrait avoir :
  ```
  AssertionError: [2,"a"] must have length of 3
  + expected - actual

      -2
      +3
  ```


[Hide-Address-Bar](https://github.com/scottjehl/Hide-Address-Bar)
-----------------------------------------------------------------

* Publication de ce code sur NPM (s’inspirer de la PR
  https://github.com/joequery/Stupid-Table-Plugin/pull/158)


[iOS-Orientationchange-Fix](https://github.com/scottjehl/iOS-Orientationchange-Fix)
-----------------------------------------------------------------------------------

* Publication de ce code sur NPM (s’inspirer de la PR
  https://github.com/joequery/Stupid-Table-Plugin/pull/158)


[Fast-csv](https://www.npmjs.com/package/fast-csv)
------------------------------------------------------

* Faire que l'option headers=Array renvoie une erreur si la première ligne du
  fichier CSV ne correspond pas à l'Array fourni (C'est peut-être corrigé avec
  les versions récentes : à valider)

* Passage à ESLint à la place de JSHint


[calc-polyfill](https://github.com/closingtag/calc-polyfill)
------------------------------------------------------------

* Réaliser les points *To Do*


[loadJS](https://github.com/filamentgroup/loadJS)
-------------------------------------------------

* Ajout des badges (npm, dependencies, devDependencies) => attribué à AAM


[loadCSS](https://github.com/filamentgroup/loadCSS)
---------------------------------------------------

* Ajout des badges (npm, dependencies, devDependencies)  => attribué à TCSO


[Scripto](https://github.com/e-henry/scripto)
---------------------------------------------------

* Effectuer une tâche de la TODO


[Atom](https://github.com/atom/atom)
---------------------------------------------------

* Prendre en charge une tâche consistant à améliorer
[la documentation](https://github.com/atom/atom/labels/documentation)


[node-gitlab](https://github.com/repo-utils/gitlab)
---------------------------------------------------

*Apparemment ce projet n'est plus maintenu*

* Documenter l'utilisation de la pagination dans tous les appels
* Ajouter un badge devDependencies
* Mettre à jour les dependencies et les devDependencies
* Ajouter un fichier .editorconfig
* Passage de JSHint à ESLint
* Correction du build failing
* Ajouter la possibilité de faire un `client.projects.search({open_issues})`


[pdf-text-extract](https://github.com/nisaacson/pdf-text-extract)
-----------------------------------------------------------------

*Apparemment ce projet n'est plus maintenu*

* Corriger le ticket
  [splitPages not working?](https://github.com/nisaacson/pdf-text-extract/issues/18)

* Ajout des badges (npm, devDependencies)
* Ajout de ESLint dans les tests
* Ajout de la couverture de tests avec ajout du badge coverage
* Mise à jour des dependencies


[LibreOffice Easy Hacks](https://wiki.documentfoundation.org/Development/Easy_Hacks)
------------------------------------------------------------------------------------

Faire des modifications faciles sur LibreOffice

Ce sujet peut être choisi par plusieurs élèves, il suffit de choisir des tâches différentes.
